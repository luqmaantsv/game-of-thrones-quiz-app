//
//  GameData.swift
//  Game Of Thrones Quiz App
//
//  Created by Luqmaan Siddiqui on 12/18/18.
//  Copyright © 2018 Luqmaan Siddiqui. All rights reserved.
//

import Foundation

enum GameDifficulty: Int {
        case easy = 1
        case medium
        case hard
    }

enum NumberOfQuestions: Int {
        case ten = 10
        case twenty = 20
        case thirty = 30
    }

struct NumberOfLives {
   public static let easyTen = 3
   public static let easyTwenty = 5
   public static let easyThirty = 7
   public static let mediumTen = 2
   public static let mediumTwenty = 4
   public static let mediumThirty = 5
   public static let hardTen = 1
   public static let hardTwenty = 3
   public static let hardThirty = 4
}
