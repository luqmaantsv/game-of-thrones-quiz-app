//
//  Constants.swift
//  Game Of Thrones Quiz App
//
//  Created by Luqmaan Siddiqui on 12/24/18.
//  Copyright © 2018 Luqmaan Siddiqui. All rights reserved.
//

import Foundation
import UIKit

public let selectColor = #colorLiteral(red: 1, green: 0.6837917343, blue: 0, alpha: 1)
public let unselectedColor = #colorLiteral(red: 1, green: 0.8323456645, blue: 0.4732058644, alpha: 1)
public let textColor = #colorLiteral(red: 0.5825562477, green: 0.1581582725, blue: 0.1006474271, alpha: 1)

public var difficulty = Int()
public var numberOfQuestions = Int()

public var score = Int()
