//
//  BadgesViewController.swift
//  Game Of Thrones Quiz App
//
//  Created by Luqmaan Siddiqui on 2/24/19.
//  Copyright © 2019 Luqmaan Siddiqui. All rights reserved.
//

import UIKit

class BadgesViewController: UIViewController {

    @IBOutlet var platinumView: UIView!
    @IBOutlet var platinumLabel: UILabel!
    @IBOutlet var platinumImageView: UIImageView!
    
    @IBOutlet var goldView: UIView!
    @IBOutlet var goldLabel: UILabel!
    @IBOutlet var goldImageView: UIImageView!
    
    @IBOutlet var silverView: UIView!
    @IBOutlet var silverLabel: UILabel!
    @IBOutlet var silverImageView: UIImageView!
    
    @IBOutlet var bronzeView: UIView!
    @IBOutlet var bronzeLabel: UILabel!
    @IBOutlet var bronzeImageView: UIImageView!
    
    @IBOutlet weak var backButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.backButton.layer.cornerRadius = 10
        self.backButton.clipsToBounds = true
        
        // check if badge is present
        if let platinumBadges = UserDefaults.standard.object(forKey: "platinum") as? Int {
            self.platinumLabel.text = " X \(platinumBadges) platinum badges"
        } else {
            self.platinumLabel.text = " X 0 platinum badges"
        }
        
        if let goldBadges = UserDefaults.standard.object(forKey: "gold") as? Int {
            self.goldLabel.text = " X \(goldBadges) gold badges"
        } else {
            self.goldLabel.text = " X 0 gold badges"
        }
        
        if let silverBadges = UserDefaults.standard.object(forKey: "silver") as? Int {
            self.silverLabel.text = " X \(silverBadges) silver badges"
        } else {
            self.silverLabel.text = " X 0 silver badges"
        }
        
        if let bronzeBadges = UserDefaults.standard.object(forKey: "bronze") as? Int {
            self.bronzeLabel.text = " X \(bronzeBadges) bronze badges"
        } else {
            self.bronzeLabel.text = " X 0 bronze badges"
        }
    }
}
