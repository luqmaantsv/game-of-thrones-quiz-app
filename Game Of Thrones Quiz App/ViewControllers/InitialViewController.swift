//
//  ViewController.swift
//  Game Of Thrones Quiz App
//
//  Created by Luqmaan Siddiqui on 12/6/18.
//  Copyright © 2018 Luqmaan Siddiqui. All rights reserved.
//

import UIKit
import GoogleMobileAds
import Crashlytics

class InitialViewController: UIViewController {
    
    @IBOutlet var adBannerView: GADBannerView!
    
    var gameOptions = [String:Int]()
    
    let difficultyKey = "difficultyKey"
    let numberOfQuestionsKey = "numberOfQuestionsKey"
    
    @IBOutlet var highScoreLabel: UILabel!
    @IBOutlet var easyButton: UIButton!
    @IBOutlet var mediumButton: UIButton!
    @IBOutlet var hardButton: UIButton!
    
    @IBOutlet var tenQuestionsButton: UIButton!
    @IBOutlet var twentyQuestionsButton: UIButton!
    @IBOutlet var threeQuestionsButton: UIButton!
    
    @IBOutlet var startButton: UIButton!
    
    @IBOutlet var outerView: UIView!
    @IBOutlet var innerView: UIView!
    
    @IBOutlet var titleLabel: UILabel!
    
    @IBOutlet weak var badgesButton: UIButton!
    
    var playersButtonArray = [UIButton]()
    var difficultyButtonArray = [UIButton]()
    var numberOfQuestionsButtonArray = [UIButton]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.adBannerView.adUnitID = "ca-app-pub-5955653176825354/7364175199"
        self.adBannerView.rootViewController = self
        self.adBannerView.load(GADRequest())
        self.titleLabel.textColor = unselectedColor
        
        self.difficultyButtonArray = [self.easyButton, self.mediumButton, self.hardButton]
        self.numberOfQuestionsButtonArray = [self.tenQuestionsButton, self.twentyQuestionsButton, self.threeQuestionsButton]
        
        self.gameOptions[difficultyKey] = GameDifficulty.easy.rawValue
        self.gameOptions[numberOfQuestionsKey] = NumberOfQuestions.ten.rawValue
        
        self.setupStartButton()
        
        self.setupButton(button: self.easyButton, self.mediumButton, self.hardButton, self.tenQuestionsButton, self.twentyQuestionsButton, self.threeQuestionsButton)
        
        self.difficultyButtonArray[0].backgroundColor = selectColor
        self.numberOfQuestionsButtonArray[0].backgroundColor = selectColor
    
        self.startButton.layer.cornerRadius = 5
        self.startButton.clipsToBounds = true
        self.startButton.backgroundColor = selectColor
        
        self.badgesButton.layer.cornerRadius = 5
        self.badgesButton.clipsToBounds = true
        self.badgesButton.backgroundColor = selectColor
        
        if let easy_10 = UserDefaults.standard.object(forKey: "1-10") as? Int {
            self.highScoreLabel.text = "Highscore:\(easy_10)"
        } else {
            self.highScoreLabel.text = ""
        }
    }
    
    func setupStartButton() {
        
        self.startButton.layer.cornerRadius = 5
        self.startButton.layer.borderWidth = 1
        self.startButton.layer.borderColor = UIColor.black.cgColor
        self.startButton.setTitleColor(textColor, for: .normal)
        
        self.badgesButton.layer.cornerRadius = 5
        self.badgesButton.layer.borderWidth = 1
        self.badgesButton.layer.borderColor = UIColor.black.cgColor
        self.badgesButton.setTitleColor(textColor, for: .normal)
    }
    
    func setupButton(button: UIButton...) {
        
        for each in button {
        each.layer.cornerRadius = 5
        each.layer.borderWidth = 1
        each.layer.borderColor = UIColor.black.cgColor
        each.setTitleColor(textColor, for: .normal)
        each.backgroundColor = unselectedColor
        each.titleLabel?.textAlignment = .center
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "quizSegue", let gameDifficulty = gameOptions[difficultyKey], let gameNumberOfQuestions = gameOptions[numberOfQuestionsKey] {
            
            if let _ = segue.destination as? QuizViewController {
                
                difficulty = gameDifficulty
                numberOfQuestions = gameNumberOfQuestions
            }
        }
    }
    
    func highLightSelectedButton( buttonArray: inout [UIButton], buttonPosition: Int) {
        
        for each in buttonArray {
            each.backgroundColor = unselectedColor
        }
        
        buttonArray[buttonPosition].backgroundColor = selectColor
    }
    
    @IBAction func difficultyButtonPressed(_ sender: UIButton) {
        
        highLightSelectedButton(buttonArray: &difficultyButtonArray, buttonPosition: difficultyButtonArray.firstIndex(of: sender)!)
        
        self.gameOptions[difficultyKey] = getDifficulty(button: sender)
        
        self.setHighScore()

    }
    
    func setHighScore() {
        
        if let gameDifficulty = gameOptions[difficultyKey], let numberOfQuestions = gameOptions[numberOfQuestionsKey]{
            let key = "\(gameDifficulty)-\(numberOfQuestions)"
            
            if let highscore = UserDefaults.standard.object(forKey: key) as? Int {
                self.highScoreLabel.text = "Highscore:\(highscore)"
            } else {
                self.highScoreLabel.text = ""
            }
        }
    }
    
    func getDifficulty(button: UIButton) -> Int {
        
        switch button {
        case mediumButton:
            return GameDifficulty.medium.rawValue
        case hardButton:
            return GameDifficulty.hard.rawValue
        default:
            return GameDifficulty.easy.rawValue
        }
    }
    
    @IBAction func questionsCountButtonPressed(_ sender: UIButton) {
        
        highLightSelectedButton(buttonArray: &numberOfQuestionsButtonArray, buttonPosition: numberOfQuestionsButtonArray.firstIndex(of: sender)!)
        
        gameOptions[numberOfQuestionsKey] = getNumberOfQuestions(button: sender)
        
        self.setHighScore()
    }
    
    func getNumberOfQuestions(button: UIButton) -> Int {
        
        switch button {
        case twentyQuestionsButton:
            return NumberOfQuestions.twenty.rawValue
        case threeQuestionsButton:
            return NumberOfQuestions.thirty.rawValue
        default:
            return NumberOfQuestions.ten.rawValue
        }
    }
}
