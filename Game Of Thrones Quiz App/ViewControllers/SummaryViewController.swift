//
//  SummaryViewController.swift
//  Game Of Thrones Quiz App
//
//  Created by Luqmaan Siddiqui on 12/19/18.
//  Copyright © 2018 Luqmaan Siddiqui. All rights reserved.
//

import UIKit
import GoogleMobileAds

class SummaryViewController: UIViewController {
    
    @IBOutlet var adBannerView: GADBannerView!
    @IBOutlet var highScoreLabel: UILabel!
    @IBOutlet var mainMenuButton: UIButton!
    @IBOutlet var restartButton: UIButton!
    @IBOutlet var scoreLabel: UILabel!
    @IBOutlet var badgeView: UIView!
    @IBOutlet var badgeLabel: UILabel!
    @IBOutlet var badgeImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.adBannerView.adUnitID = "ca-app-pub-5955653176825354/7364175199"
        self.adBannerView.rootViewController = self
        self.adBannerView.load(GADRequest())
        self.mainMenuButton.layer.cornerRadius = 10
        self.mainMenuButton.clipsToBounds = true
        
        self.restartButton.layer.cornerRadius = 10
        self.restartButton.clipsToBounds = true
        
        self.scoreLabel.text = "Score: \(score)"
        
        let key = "\(difficulty)-\(numberOfQuestions)"
        
        addBadge(score: score)
        
        if let highscore = UserDefaults.standard.object(forKey: key) as? Int, highscore > score {
            self.highScoreLabel.text = "Highscore:\(highscore)"
        } else {
            self.highScoreLabel.text = "Highscore:\(score)"
            UserDefaults.standard.set(score, forKey: key)
        }
    }
    
    @IBAction func restartButtonTapped(_ sender: Any) {
        self.performSegue(withIdentifier: "restartSegue", sender: self)
    }
    
    @IBAction func mainMenuButtonTapped(_ sender: Any) {
        self.performSegue(withIdentifier: "mainMenuSegue", sender: self)
    }
    
    func updateTotalBadges(badge:String) {
        if let platinumBadge = UserDefaults.standard.object(forKey: badge) as? Int {
            let scoreIncre = platinumBadge + 1
            UserDefaults.standard.set(scoreIncre, forKey: badge)
        } else {
            UserDefaults.standard.set(1, forKey: badge)
        }
    }
    
    func addBadge(score: Int) {
        
        switch score {
        case let n where n == numberOfQuestions:
            self.badgeImageView.image = UIImage(named: "sevenStar")
            self.badgeLabel.text = "Platinum badge"
            self.updateTotalBadges(badge: "platinum")
            break
        case let n where (n >= 0 && n < numberOfQuestions/4):
            self.badgeView.isHidden = true
            break
        case let n where (n >= numberOfQuestions/4 && n < numberOfQuestions/2):
            self.badgeImageView.image = UIImage(named: "fourStar")
            self.badgeLabel.text = "Bronze badge"
            self.updateTotalBadges(badge: "bronze")
            break
        case let n where (n >= numberOfQuestions/2 && n < numberOfQuestions*(3/4)):
            self.badgeImageView.image = UIImage(named: "fiveStar")
            self.badgeLabel.text = "Silver badge"
            self.updateTotalBadges(badge: "silver")
            break
        case let n where (n >= numberOfQuestions*(3/4) && n < numberOfQuestions - 1):
            self.badgeImageView.image = UIImage(named: "sixStar")
            self.badgeLabel.text = "Gold badge"
            self.updateTotalBadges(badge: "gold")
            break
        default:
            print("trying to get badge \(score), \(numberOfQuestions)")
            self.badgeView.isHidden = true
        }
    }
}
