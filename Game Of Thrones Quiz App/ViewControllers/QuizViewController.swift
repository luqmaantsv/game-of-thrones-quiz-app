//
//  QuizViewController.swift
//  Game Of Thrones Quiz App
//
//  Created by Luqmaan Siddiqui on 12/8/18.
//  Copyright © 2018 Luqmaan Siddiqui. All rights reserved.
//

import UIKit
import Crashlytics
import GoogleMobileAds

class QuizViewController: UIViewController {
    
    @IBOutlet var questionLabel: UILabel!
    @IBOutlet var scoresLabel: UILabel!
    @IBOutlet var questionsNumberLabel: UILabel!
    
    @IBOutlet var optionAButton: UIButton!
    @IBOutlet var optionBButton: UIButton!
    @IBOutlet var optionCButton: UIButton!
    @IBOutlet var optionDButton: UIButton!
    
    @IBOutlet var quitButton: UIButton!
    
    @IBOutlet var heartSeven: UIImageView!
    @IBOutlet var heartSix: UIImageView!
    @IBOutlet var heartFive: UIImageView!
    @IBOutlet var heartFour: UIImageView!
    @IBOutlet var heartThree: UIImageView!
    @IBOutlet var heartTwo: UIImageView!
    @IBOutlet var heartOne: UIImageView!
    
    @IBOutlet weak var adView: GADBannerView!
    
    var allHeartsArray = [UIImageView]()
    var gameHeartsArray = [UIImageView]()
    var optionsButtonsArray = [UIButton]()
    
    let crossHeartsImage = UIImage(named: "crossHeart.png")
    let heartsImage = UIImage(named: "heart.png")
    
    var currentQuestionCount = 1
    var currentScore = 0
    var questionNumber = Int()
    var numberOfLives = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.allHeartsArray = [heartOne, heartTwo, heartThree, heartFour, heartFive, heartSix, heartSeven]
        self.quitButton.layer.cornerRadius = 10
        self.quitButton.clipsToBounds = true
        
        self.adView.adUnitID = "ca-app-pub-5955653176825354/7364175199"
        self.adView.rootViewController = self
        self.adView.load(GADRequest())
        
        for each in allHeartsArray {
            each.image = UIImage(named: "heart.png")
            each.isHidden = true
        }
        
        self.scoresLabel.text = "Score: 0"
        self.calculateNumberOfLives()
        
        self.optionsButtonsArray = [optionAButton, optionBButton, optionCButton, optionDButton]
        
        self.optionAButton.tag = 1
        self.optionBButton.tag = 2
        self.optionCButton.tag = 3
        self.optionDButton.tag = 4
        
        for each in self.optionsButtonsArray {
            each.layer.cornerRadius = 10
            each.clipsToBounds = true
        }
        
        for i in 0...numberOfLives - 1 {
            self.gameHeartsArray.append(allHeartsArray[i])
            self.gameHeartsArray[i].isHidden = false
        }
        
        if let questionNumber = UserDefaults.standard.object(forKey: "questionNumber") as? Int {
            self.questionNumber = questionNumber
        } else {
            self.questionNumber = 0
        }
        
        self.loadQuestion()
    }
    
    func calculateNumberOfLives() {
        let difficultyNumberOfLives = (difficulty, numberOfQuestions)
        
        switch difficultyNumberOfLives {
        case (1,10):
            numberOfLives = NumberOfLives.easyTen
        case (1,20):
            numberOfLives = NumberOfLives.easyTwenty
        case (1,30):
            numberOfLives = NumberOfLives.easyThirty
        case (2,10):
            numberOfLives = NumberOfLives.mediumTen
        case (2,20):
            numberOfLives = NumberOfLives.mediumTwenty
        case (2,30):
            numberOfLives = NumberOfLives.mediumThirty
        case (3,10):
            numberOfLives = NumberOfLives.hardTen
        case (3,20):
            numberOfLives = NumberOfLives.hardTwenty
        case (3,30):
            numberOfLives = NumberOfLives.hardThirty
        default:
            break
        }
    }
    
    func loadQuestion() {
        
        if (self.questionNumber >= blah.count) {
            self.questionNumber = 0
        }
        
        self.questionsNumberLabel.text = "\(self.currentQuestionCount)/\(numberOfQuestions)"
        
        for each in optionsButtonsArray {
            each.layer.removeAllAnimations()
            each.backgroundColor = selectColor
        }
        
        self.enableButtons()
        
        self.questionLabel.text = blah[self.questionNumber].question
        self.optionAButton.setTitle(blah[self.questionNumber].optionA, for: .normal)
        self.optionBButton.setTitle(blah[self.questionNumber].optionB, for: .normal)
        self.optionCButton.setTitle(blah[self.questionNumber].optionC, for: .normal)
        self.optionDButton.setTitle(blah[self.questionNumber].optionD, for: .normal)
    }
    
    @IBAction func closeButton(_ sender: Any) {
        self.performSegue(withIdentifier: "quizToHomeScreen", sender: self)
    }
    
    func endGame() {
        self.questionNumber += 1
        UserDefaults.standard.set(self.questionNumber, forKey: "questionNumber")
        print(numberOfQuestions)
        score = currentScore
        self.performSegue(withIdentifier: "summarySegue", sender: self)
    }
    
    func updateScore() {
        self.scoresLabel.text = "Score: \(self.currentScore)"
    }
    
    @IBAction func buttonPressed(_ sender: UIButton) {
        
        var correctButton = UIButton()
        
        self.disableButtons()
        if sender.tag == blah[self.questionNumber].correctAnswer {
            sender.backgroundColor = UIColor.green
            UIView.animate(withDuration: 0.3, delay: 0.0, options: UIView.AnimationOptions.repeat, animations: {
                sender.alpha = 0.0
            }, completion: nil)
            self.currentScore += 1
            self.updateScore()
            
        } else {
            sender.backgroundColor = UIColor.orange
            gameHeartsArray[numberOfLives-1].image = crossHeartsImage
            numberOfLives -= 1
            for each in self.optionsButtonsArray {
                if each.tag == blah[self.questionNumber].correctAnswer {
                    each.backgroundColor = UIColor.green
                    correctButton = each
                    UIView.animate(withDuration: 0.3, delay: 0.0, options: UIView.AnimationOptions.repeat, animations: {
                        correctButton.alpha = 0.0
                    }, completion: nil)
                }
            }
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now()+1) {
            sender.alpha = 1.0
            correctButton.alpha = 1.0
            
            if self.currentQuestionCount == numberOfQuestions || self.numberOfLives == 0 {
                self.endGame()
            } else {
                self.currentQuestionCount += 1
                self.questionNumber += 1
                UserDefaults.standard.set(self.questionNumber, forKey: "questionNumber")
                self.loadQuestion()
            }
        }
    }
    
    func disableButtons() {
        optionAButton.isEnabled = false
        optionDButton.isEnabled = false
        optionBButton.isEnabled = false
        optionCButton.isEnabled = false
    }
    
    func enableButtons() {
        optionAButton.isEnabled = true
        optionDButton.isEnabled = true
        optionBButton.isEnabled = true
        optionCButton.isEnabled = true
    }
}
